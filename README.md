# skaffold
Gitlab CI Config
- Under Settings -> CICD add the varaible `KUBE_CONFIG` with output of `cat okteto-kube.config | base64`

## Local
### Install Skafold
```
curl -Lo skaffold https://storage.googleapis.com/skaffold/releases/latest/skaffold-linux-amd64
sudo install skaffold /usr/local/bin/
```
### Spinning up environment
```
skaffold dev --status-check=false
```

- echo $(cat ~/.kube/config | base64) | tr -d " "